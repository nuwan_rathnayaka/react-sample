import React from 'react';
import './Toolbar.css';
import DrawerToggleButton from "../Sidedrawer/DrawerToggle";
import MenuItem from './ToolbarItem/item';
// import logo from '../../../public/logo.svg';
import {Typeahead}from 'react-bootstrap-typeahead';

const toolbar = props => (
    <header className="toolbar">
        <nav className="toolbar-navigation">
            {/*<div className="toolbar-toggle-button">*/}
            {/*    <DrawerToggleButton click={props.drawerClickHandler}/>*/}
            {/*</div>*/}
            {/*<div className="toolbar-logo"><a href="/"><img width="120px" height="30px" src={logo}/></a></div>*/}
            <div className="spacer"/>
            <Typeahead id="type-head-search" className="typehead-styles"
                labelKey={(option) => `${option.tag1} ${option.tag2} ${option.tag3}`}
                options={[
                    {tag1: 'Art', tag2: 'Blakey',tag3:'adidas'},
                    {tag1: 'John', tag2: 'Coltrane',tag3:'nike'},
                    {tag1: 'Miles', tag2: 'Davis',tag3:'asos'},
                    {tag1: 'Herbie', tag2: 'Hancock',tag3:'adidas'},
                    {tag1: 'Charlie', tag2: 'Parker',tag3:'ck'},
                    {tag1: 'Tony', tag2: 'Williams',tag3:'ck'},
                ]}
                placeholder="Search items here..."
            />
            <button type="button" className="btn btn-outline-light">Search</button>
        </nav>
    </header>
)

export default toolbar;
