import React from 'react'
import './item.css'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAmbulance} from "@fortawesome/free-solid-svg-icons";

const MenuItem= props=>{
    return (
        <div className="dropdown">
            <a className="dropdownitem" href="/">{props.item_name}</a>
            <div className="dropdown-content">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <FontAwesomeIcon icon={faAmbulance} size="lg"></FontAwesomeIcon><h3>Men</h3>
                            <ul className="items-ul">
                                <li className="items-li"><a className="item-a" href="/">Shirt</a></li>
                                <li className="items-li"><a className="item-a" href="/">Denim</a></li>
                                <li className="items-li"><a className="item-a" href="/">Chinos & Pants</a></li>
                                <li className="items-li"><a className="item-a" href="/">Underwear</a></li>
                                <li className="items-li"><a className="item-a" href="/">Belts</a></li>
                                <li className="items-li"><a className="item-a" href="/">Wallets</a></li>
                                <li className="items-li"><a className="item-a" href="/">link</a></li>
                            </ul>
                        </div>
                        <div className="col-md-4">
                            <h3>Women</h3>
                            <FontAwesomeIcon icon={faAmbulance} size="lg"></FontAwesomeIcon>
                            <a href="/">link</a>
                            <a href="/">link</a>
                        </div>
                        <div className="col-md-4">
                            <FontAwesomeIcon icon={faAmbulance} size="lg"></FontAwesomeIcon>
                            <a href="/">link</a>
                            <a href="/">link</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
}

export default MenuItem;
