import React from 'react';
import "./SideDrawer.css";
import MenuItem from '../Toolbar/ToolbarItem/item';

const sideDrawer = props => {
    let drawerClasses='side-drawer';
    if(props.show){
        drawerClasses='side-drawer open';
    }
    return (
        <nav className={drawerClasses}>
            <ul>
                <li>
                    {/*<a href="/">User</a>*/}
                    <MenuItem item_name="User"/>
                </li>
                <li>
                    {/*<a href="/">Home</a>*/}
                    <MenuItem item_name={"Profile"}/>
                </li>
            </ul>
        </nav>);
};

export default sideDrawer;
