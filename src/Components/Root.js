import React from 'react'
import Toolbar from "./Toolbar/Toolbar";
import SearchBar from "./SearchBar/SearchBar";
import SideDrawer from "./Sidedrawer/SideDrawer";
import BackDrop from "./BackDrop/Backdrop";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Home from "./Home";
import MenuBar from "./MenuBar/MenuBar";
import Products from "../home-components/home/products/products";

class Root extends React.Component {
    state = {
        sideDrawerOpen: false
    }
    drawerToggleClickHandler = () => {
        this.setState((prevState) => {
            return {sideDrawerOpen: !prevState.sideDrawerOpen}
        })
    }
    backdropClickHandler = () => {
        this.setState({sideDrawerOpen: false})
    }

    render() {
        let backDop;
        if (this.state.sideDrawerOpen) {
            backDop = <BackDrop click={this.backdropClickHandler}/>;
        }
        return (
            <div style={{height: '100%'}}>
                {/*<Toolbar drawerClickHandler={this.drawerToggleClickHandler}/>*/}
                <MenuBar drawerClickHandler={this.drawerToggleClickHandler}/>
                <SideDrawer show={this.state.sideDrawerOpen}/>
                {backDop}
                <main style={{marginTop: '100px'}}>
                    <Router>
                        <Switch>
                            <Route path="/about" component={Home}/>
                            <Route path="/products" component={Products}/>
                        </Switch>
                    </Router>
                </main>
            </div>
        )
    }

}

export default Root;
