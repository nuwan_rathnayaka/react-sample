import React from 'react';
import './MenuBar.css';
import MenuItem from "../Toolbar/ToolbarItem/item";
import DrawerToggleButton from "../Sidedrawer/DrawerToggle";
import logo from "./logo.svg";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import {faUser} from "@fortawesome/free-solid-svg-icons";
// import DrawerToggleButton from "../Sidedrawer/DrawerToggle";
// import MenuItem from './ToolbarItem/item';
// import logo from '../../../public/logo.svg';

const menubar = props => (
    <header className="menubar">
        <nav className="menubar-navigation">
            <div className="toolbar-toggle-button">
                <DrawerToggleButton click={props.drawerClickHandler}/>
            </div>
            <div className="menubar-navigation-items">

                <div className="toolbar-logo"><a href="/"><img width="120px" height="30px" src={logo}/></a></div>
                <div className="spacer"/>
                <ul>
                    <li><MenuItem item_name="Clothes"/></li>
                    <li><MenuItem item_name="Accessories"/></li>
                    <li><MenuItem item_name="Shoes"/></li>
                    <li><MenuItem item_name="Body & Care"/></li>
                    <li><MenuItem item_name="Other"/></li>
                </ul>

            </div>
            <div className="right-icon-div">
                {/*right side menu*/}
                {/*<FontAwesomeIcon className="icons-right" icon={faUser} />*/}
                <i className="fa fa-user-o"></i>
            </div>
        </nav>
    </header>
)

export default menubar;
