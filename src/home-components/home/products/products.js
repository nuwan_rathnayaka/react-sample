import React,{Component} from 'react';
import MenuBar from "../../../Components/MenuBar/MenuBar";
import Toolbar from "../../../Components/Toolbar/Toolbar";


class Products extends Component{
    render(){
        return(
            <div>
                <Toolbar drawerClickHandler={this.drawerToggleClickHandler}/>
                Products
            </div>
        )
    }

}
export default Products;
